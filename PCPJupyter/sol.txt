Instance 1:
3 4 276 1 1.633477
1101  01    1     
010   1     101   

Solvable!

 0 0 1
 0 1 0
Gcd: 1
Original Direction: upmask 
Reverse Direction: downmask 
Visited node in original direction: 140
Visited node in reverse direction: 85
Choose the reverse direction:
Find the solution in depth: 276 (depth threshold: 280)
  2  1  2  2  2  1  2  1  2  2  2  1  2  2  3  1  3  2  2  1
  2  3  2  2  3  1  2  2  3  2  1  2  3  2  2  1  2  2  1  2
  2  3  2  1  3  2  3  1  2  2  2  1  2  2  1  2  2  2  3  3
  2  3  1  3  2  2  1  3  2  2  1  3  2  3  3  2  2  3  1  2
  2  2  3  1  2  2  2  3  2  2  1  2  3  2  3  1  2  3  2  3
  1  2  3  1  2  2  3  3  3  2  3  3  3  2  3  3  2  2  1  2
  2  2  3  2  2  3  2  3  1  2  2  3  1  2  2  1  2  3  3  2
  2  1  2  3  2  2  1  3  2  3  2  2  1  3  2  3  3  1  2  2
  2  3  3  1  2  2  2  3  2  3  2  2  1  3  2  3  2  2  1  3
  3  3  1  2  2  2  3  3  1  2  2  2  2  2  3  2  3  1  2  2
  3  2  2  1  3  1  2  3  3  2  2  1  2  3  1  2  2  2  3  2
  3  2  2  1  3  2  3  3  2  3  1  2  3  3  1  2  2  2  3  2
  3  3  2  3  2  3  2  3  1  2  3  2  3  3  3  3  2  3  3  2
  2  2  3  2  3  1  2  3  3  2  3  2  3  3  2  3
Time spent: 1.633477 seconds.


visited node: 22313164, last iteration: 11841068
cutoff node : 18005, last iteration: 10041
time: 1.633477, last iteration: 0.907030
Search speed: 13.660M/s, laster iteration: 13.055M/s

Total nodes searched: 22313164

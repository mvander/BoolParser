PCPJupyter

This program is a simple interface between python and Ling Zhao's PCP solver (https://github.com/chrozz/PCPSolver). For details on usage, please see the Jupyter Notebook file PCP Jupyter.

The program essentially just forwards PCP instances to the solver, then interprets the result and prints them in a readable fashion (just prints the solution if it exists and shows the actual string). Input should be in the form of a list duples, each representing a tile. The first string should be the top of the tile, the second the bottom. You can also provide arguments to the solver. Again, please see the Jupyter notebook file for more information.

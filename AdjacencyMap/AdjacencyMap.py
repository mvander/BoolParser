# AdjacencyMap.py
# Use OpenStreetMap's Overpass API to get adjacency lists for maps.

import sys
sys.path.append("overpy/")

from overpy.__init__ import *

api = Overpass(read_chunk_size=None, url=None, xml_parser=XML_PARSER_SAX, max_retry_count=3, retry_timeout=5.0)


def get_adjacency_map(area, region_type, admin_level):
    """
    Grab the adjacency list of the map.
    """
    result = api.query("""
    [out:json];
    (area["name"="{area}"];area["name:en"="{area}"];);
    (node[place="{region_type}"](area););
    out;
    """.format(area=area, region_type=region_type))

    regions = list(map(lambda node: node.tags["name"], result.get_nodes()))

    done_list = list(regions)
    adj_dict, retry_list = _get_adjacents_helper({}, regions, regions, [], area, region_type, admin_level)

    while len(retry_list) != 0:
        retry_list = list(set(retry_list))
        done_list = list(set(retry_list + done_list))
        adj_dict, retry_list = _get_adjacents_helper(adj_dict, done_list, retry_list, [], area, region_type, admin_level)

    return adj_dict


def _get_adjacents_helper(adj_dict, done, current_list, retry_list, area, region_type, admin_level):
    """
    Recursive helper to get the adjacency list for a given area, region type, and admin-level.
    :param adj_dict: adjacency list so far (actually a dictionary).
    :param current_list: current list of regions we are checking.
    :param retry_list: growing list of values to retry.
    :return: adjacency list with items on list added.
    """
    for region in current_list:
        adj_dict[region] = get_adjacents(area, region_type, region, admin_level)
        # Add any we get back that aren't in our original list to a list we will error check.
        new_retries = [x for x in adj_dict[region] if x not in done]
        print(str(new_retries))
        retry_list.extend(new_retries)
        retry_list = list(set(retry_list))

    return adj_dict, retry_list


def get_adjacents(area, region_type, region, admin_level):
    """
    For the given region, provides it's adjacent regions.
    """
    print(region)

    result = api.query("""
    [out:json];
    (area["name"="{area}"];area["name:en"="{area}"];);
    (node[place="{region_type}"][name="{region}"](area);) -> .u;
    .u is_in -> .v;
    area.v[admin_level={admin_level}] -> .w;
    rel(area.w)[admin_level={admin_level}] -> .a;
    .a > -> .b;
    way.b[boundary="administrative"][admin_level={admin_level}] -> .c;
    .c < -> .d;
    rel.d[admin_level={admin_level}] -> .e;
    .e out tags;
    """.format(region=region, area=area, admin_level=admin_level, region_type=region_type))

    print(len(result.get_relations()))

    adjacents = []
    for adjacent in result.get_relations():
        adjacents.append(adjacent.tags["name"].replace(" County", ""))

    final_adjacents = list(filter(lambda x: x != region, adjacents))

    return final_adjacents

Adjacency Map

This is a simple python program that uses the Overpass API for OSM (Open Street Map) to generate Adjacency Maps in the
form of dictionaries, mapping a region to all adjacent regions.

Usage:

get_adjacency_map(area, region_type, admin_level) - this is the main function that will actually create the dictionary.

    area - the surrounding area for the adjacency map (ex., USA for adjacency map of the 50 states).
    region_type - the type of region within the area you are looking for (ex., state for US states adjacency map).
    admin_level - this is a single number (as a string) that represents the administrative level of the boundary
        associated with the region_type we are using. The best way to use this is to simply look it up on the
        table at the bottom of this site: http://wiki.openstreetmap.org/wiki/Tag:boundary%3Dadministrative.

This will return a dictionary accordingly. Note: because of the open-source nature of OSM, data is not fully
standardized which makes it difficult to write a perfect OSM adjacency map collector for all situations. In an
attempt to offset these inaccuracies, my program will continuously retry on any regions found to be adjacent to ones
in the original list of regions of the type we are looking for in the area. Trials have shown that this allows for a
more complete final mapping. This may however cause items to be added that shouldn't be in the mapping. Here, I choose
to under-correct and let the user ignore parts they do not want to use. The easiest way to do this is to use
GraphColoring - it will raise errors when there are inconsistencies in the adjacency dictionaries which one can easily
correct.

Description of Overpass Queries:

The Overpass queries are hard to read, so here I will try to describe the general usage. For anyone looking to toy
around with this, I suggest using Overpass Turbo (https://overpass-turbo.eu) which allows you to write, run, and
visualize calls made to the API quickly and easily.

Call to Get List of Regions within Area: get the list of regions of the provided type from which we will form an
adjacency list.

[out:json];                                         # Specifies that the results should be JSON (not strictly necessary
                                                    # as we use Overpy (Overpass python API) to abstract many details.
(area["name"="{area}"];area["name:en"="{area}"];);  # Here, we find the area associated with the provided area name.
(node[place="{region_type}"](area););               # Now, we find all regions of the specified type we want.
out;                                                # This signifies that we return the list of regions created above.

Call to Get Adjacent Regions for a single Region: get the list of adjacent regions for the specific region. This
requires some finicking but essentially we find the total area (ex: USA), then a single sub region (ex: Utah). Then
we find the associated area (a different OSM construct) for that sub region. From there we find the relations (which is
a relationship of some kind), filtering to relationships of the correct admin_level. From here, we find all the ways
(set of nodes in a relationship) of the correct level. From there we go back to relations of that level. All this
essentially goes from the region to it's border at the correct level, then filters to find the adjacent regions based
on their relationship on the border. Its weird and complicated but somehow it works...

[out:json];                                           # As before, specify output as JSON.
(area["name"="{area}"];area["name:en"="{area}"];);    # Again, define the area.
(node[place="{region_type}"][name="{region}"](area);) -> .u; # Find the specific subregion in question and put it is .u
.u is_in -> .v;                                       # Find all areas containing our node.
area.v[admin_level={admin_level}] -> .w;              # Filter out only areas of the admin_level we are looking for.
rel(area.w)[admin_level={admin_level}] -> .a;         # Find any relations in the area of the admin_level looked for.
                                                      # Relations define logical or geographical relationships.
.a > -> .b;                                           # Recurse down - find all nodes/ways that are members of the
                                                      # relation in .a
way.b[boundary="administrative"][admin_level={admin_level}] -> .c; # Filter out the ways of the correct type
                                                                   # (administrative) and admin_level.
.c < -> .d;                                           # Recurse up - relations in ways from .c.
rel.d[admin_level={admin_level}] -> .e;               # Filter out to correct admin_level.
.e out tags;                                          # Return according.
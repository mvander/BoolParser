# GraphColoring.py
#
# Use code provided in Prof. Ganesh's book to generate graph colorings using Z3 Solver.

from z3 import *
import pdb
from maps import *


def ChkGraphIsSymmetric(gr):
    """ Checks if gr is symmetric; i.e. for all S1 -> S2, i.e. connected to S2,
    we also have S2 -> S1. """
    for k in gr.keys():
        for j_adj_to_k in gr[k]:
            # print "State " + k + " -> " + j_adj_to_k
            if not (k in gr[j_adj_to_k]):
                gr[k].remove(j_adj_to_k)


def ConvertToCorrectEncoding(adjacency_map):
    """
    Simple brute force algorithm that converts to an encoding that Z3 can use. The standard encoding doesn't work
    with it's Ints() call.
    """
    # Create a new adjacency map with the correct encodings.
    dict_map = {}

    for k in adjacency_map.keys():
        adjacents = []

        for j_adj_to_k in adjacency_map[k]:
            adjacents.append(j_adj_to_k.encode('utf-8'))

        dict_map[k.encode('utf-8')] = adjacents

    return dict_map


def PrintColoring(adjacency_map, color_max = 3):
    adjacency_map = ConvertToCorrectEncoding(adjacency_map)

    ChkGraphIsSymmetric(adjacency_map)

    stLstWest = list(adjacency_map.keys())

    NStates = len(stLstWest)

    LInts = Ints(stLstWest)    # Creates constraint vars ; L[0] is int var WA, etc.

    stNamIntDict = {stLstWest[i]: LInts[i] for i in range(NStates)}
    
    s = Solver()
    for state in stLstWest:
        # Default color range constraints
        s.add(stNamIntDict[state] >= 0)
        s.add(stNamIntDict[state] <= color_max)
        # These are the graph coloring constraints
        s.add(And([stNamIntDict[state] != stNamIntDict[adjState]
                               for adjState in adjacency_map[state]]))
    rslt = s.check()

    if rslt == sat:
                  print("The coloring of states is")
                  print(s.model())
    else:
       print("There is no " + str(color_max) + " coloring for the given graph.")
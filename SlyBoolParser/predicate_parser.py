# predicate_parser.py
#
# Proposition parser is a sly implementation that handles the parsing of predicate logic statements.

from sly import Parser
from bool_lexer import BoolLexer
import pdb

class PredicateParser(Parser):

    # Debug files.
    debugfile = "parser.out"

    # Parse user input.
    def parse_string(self, user_input):
        lexer = BoolLexer()
        self.user_input = user_input
        return self.parse(lexer.lex(user_input))
    
    # Grab tokens list from our lexer.
    tokens = BoolLexer.tokens
    
    # Add precedence rules (further down in the table, higher precedence).
    precedence = (
        ("nonassoc", "EQUALS", "LESST", "LESSEQT", "GREATEQT", "GREATT", "IMPLIES"),
        ("left", "OR", "NEGATE", "PLUS"),
        ("left", "MULTIPLY", "DIVIDE", "AND"),
        ("right","NOT", "UMINUS")
    )

    # Multiple types of predicates may be realized, thus we abstract to "statement" first.
    @_("predicate")
    def statement(self, p):
        return ("STATEMENT", p.predicate)


    # PREDICATES - the basis of our different types of first-order logic statements.

    # Fully defined predicate - specify functions, variables, and constraints.
    @_("functions SEMICOLON quantifiers COLON constraints")
    def predicate(self, p):
        return ("PREDICATE_FUNCTIONS", p.quantifiers, p.constraints, p.functions)

    # Specify variables but not functions.
    @_("quantifiers COLON constraints")
    def predicate(self, p):
        return ("PREDICATE", p.quantifiers, p.constraints)

    # Specify functions but not variables.
    @_("functions SEMICOLON constraints")
    def predicate(self, p):
        return ("PREDICATE_JUST_FUNCTIONS", p.functions, p.constraints)

    # Partial specification of variables (i.e., Forall or Exists on variables, but not type - allow for uninterpreted domains).
    @_("part_quantifiers COLON constraints")
    def predicate(self, p):
        return ("UNINTERP_PREDICATE", p.part_quantifiers, p.constraints)

    # Partial specification of variables with formula specifications.
    @_("functions SEMICOLON part_quantifiers COLON constraints")
    def predicate(self, p):
        return ("UNINTERP_PREDICATE_FUNCTIONS", p.part_quantifiers, p.constraints, p.functions)

    # Simplest definition of a predicate - simply a constraint, and interpret it ourselves.
    @_("constraints")
    def predicate(self, p):
        return ("CONSTRAINT_PREDICATE", p.constraints)


    # PREDICATE COMBINATIONS:

    @_("LPARENTH predicate RPARENTH")
    def predicate(self, p):
        return p.predicate

    @_("predicate IMPLIES predicate",
       "predicate AND predicate",
       "predicate OR predicate")
    def predicate(self, p):
        return ("PREDICATES_" + str(p[1]), p.predicate0, p.predicate1)

    @_("NOT predicate")
    def predicate(self, p):
        return ("PREDICATE_!", p.predicate)


    # FUNCTION SPECIFICATIONS:

    @_("function COMMA functions")
    def functions(self, p):
        return ("FUNCS", p.function, p.functions)

    @_("function")
    def functions(self, p):
        return p.function

    @_("VAR COLON range ARROW range")
    def function(self, p):
        return ("FUNC_1", p[0], p.range0, p.range1)

    @_("VAR COLON ARROW range")
    def function(self, p):
        return ("FUNC_0", p[0], p.range)

    
    # QUANTIFIERS:

    @_("quantifier")
    def quantifiers(self, p):
        return p.quantifier

    @_("quantifier COMMA quantifiers")
    def quantifiers(self, p):
        return ("QUANTIFIERS", p.quantifier, p.quantifiers)

    @_("FORALL variable IN range",
       "EXISTS variable IN range")
    def quantifier(self, p):
        return ("QUANTIFIER", p[0], p.variable, p.range, (p.lineno, p.index))

    # The following are uninterpreted predicates - that is, their functions/variables are not explicitly defined.
    # Thus we start with partial quantifiers that are not strongly typed.
    @_("FORALL variable",
       "EXISTS variable")
    def part_quantifier(self, p):
        return ("PARTIAL_QUANTIFIER", p[0], p.variable, (p.lineno, p.index))

    @_("part_quantifier")
    def part_quantifiers(self, p):
        return p.part_quantifier

    @_("part_quantifier COMMA part_quantifiers")
    def part_quantifiers(self, p):
        return ("PARTIAL_QUANTIFIERS", p.part_quantifier, p.part_quantifiers)


    # CONSTRAINTS:

    @_("constraint COMMA constraints")
    def constraints(self, p):
        return ("CONSTRAINTS", p.constraint, p.constraints)

    @_("constraint")
    def constraints(self, p):
        return p.constraint

    # This represents a function call if it is a predicate function (i.e., returns a boolean).
    @_("funccall")
    def constraint(self, p):
        return ("CONST_FUNC_CALL", p.funccall)

    @_("varexpr LESST varexpr",
       "varexpr LESSEQT varexpr",
       "varexpr GREATT varexpr",
       "varexpr GREATEQT varexpr",
       "varexpr EQUALS varexpr")
    def constraint(self, p):
        return ("CONST_RR_" + str(p[1]), p.varexpr0, p.varexpr1)

    # Below lets us combine constraints logically (note and is implied if comma-separated constraints).
    @_("constraint AND constraint",
       "constraint OR constraint",
       "constraint IMPLIES constraint")
    def constraint(self, p):
        return ("CONST_LOG_" + str(p[1]), p.constraint0, p.constraint1)

    @_("NOT constraint")
    def constraint(self, p):
        return ("CONST_LOG_!", p.constraint)

    @_("LPARENTH constraint RPARENTH")
    def constraint(self, p):
        return p.constraint


    # FUNCTION CALLS:

    @_("variable LPARENTH variable RPARENTH",
       "variable LPARENTH varexpr RPARENTH")
    def funccall(self, p):
        return ("FUNCCALL_VAR", p[0], p[2], (p.lineno, p.index))

    @_("variable LPARENTH boolean RPARENTH")
    def funccall(self, p):
        return ("FUNCCALL_CONSTANT", p.variable, p[2], (p.lineno, p.index))

    @_("variable LPARENTH RPARENTH")
    def funccall(self, p):
        return ("FUNCCALL_ZERO_ARITY", p.variable, (p.lineno, p.index))

    @_("variable LPARENTH funccall RPARENTH")
    def funccall(self, p):
        return ("FUNCCALL_IN_FUNCCALL", p.variable, p.funccall, (p.lineno, p.index))

    
    # VARIABLE EXPRESSIONS - this represents any expressions involving variables.

    @_("NUMBER")
    def varexpr(self, p):
        return ("NUMBER", p[0])

    @_("variable")
    def varexpr(self, p):
        return ("VAREXPR_VAR", p.variable)

    @_("funccall")
    def varexpr(self, p):
        return ("VE_FUNC_CALL", p.funccall)

    @_("LPARENTH varexpr RPARENTH")
    def varexpr(self, p):
        return p.varexpr

    @_("varexpr PLUS varexpr",
       "varexpr NEGATE varexpr",
       "varexpr MULTIPLY varexpr",
       "varexpr DIVIDE varexpr")
    def varexpr(self, p):
        return ("VE_COMB_VE" + str(p[1]), p.varexpr0, p.varexpr1)

    @_("NEGATE varexpr %prec UMINUS")
    def varexpr(self, p):
        return ("VAREXPR_NEGATE", p.varexpr)

    
    # BASIC TYPES - define a few basic types, such as range for representing a function domain/variable type and booleans and variables.

    @_("VAR")
    def variable(self, p):
        return ("VARIABLE", p[0])

    @_("TRUE",
       "FALSE")
    def boolean(self, p):
        if p[1] == "true":
            return ("BOOLEAN", True)
        else:
            return ("BOOLEAN", False)

    @_("INT",
       "REAL",
       "BOOL")
    def range(self, p):
        return ("RANGE", p[0])

    
    # ERROR:
    
    def error(self, p):
        # If error occurs during the string, we"re pretty much done for because they only write one full statement, so we
        # raise an exception after the error message.
        
        input_as_array = self.user_input.split("\n")

        if p:
            print("Syntax error. Unexpected token found:")
            print(input_as_array[p.lineno - 1])
            print(" " * (p.index - 1) + "^")
            
            raise Exception()
        else:
            print("Syntax error at EOF. Statement not completed.")

# bool_lexer.py
#
# This file contains the lexer for our parsers, and is used for parsing both propositional and first-order logic
# statements.

from sly import Lexer
import pdb


class BoolLexer(Lexer):

    def lex(self, user_input):
        self.user_input = user_input
        return self.tokenize(user_input)

    # Reserved keywords
    reserved = {
        'true': 'TRUE',
        'false': 'FALSE',
        'Z': 'INT',
        'R': 'REAL',
        'Bool': 'BOOL',
        'in': 'IN'
    }

    # Valid tokens
    tokens = {
                 'NEGATE',
                 'NOT',
                 'AND',
                 'OR',
                 'PLUS',
                 'IMPLIES',
                 'VAR',
                 'NUMBER',
                 'LPARENTH',
                 'RPARENTH',
                 'MULTIPLY',
                 'DIVIDE',
                 'EQUALS',
                 'LESST',
                 'GREATT',
                 'LESSEQT',
                 'GREATEQT',
                 'COLON',
                 'COMMA',
                 'FORALL',
                 'EXISTS',
                 'SEMICOLON',
                 'ARROW',
                 *set(reserved.values()),
             }

    # Regex for simple tokens:
    ARROW = r'->'
    NEGATE = r'-'
    NOT = r'\~'
    AND = r'\&'
    OR = r'\|'
    PLUS = r'\+'
    IMPLIES = r'\=\>'
    LPARENTH = r'\('
    RPARENTH = r'\)'
    MULTIPLY = r'\*'
    DIVIDE = r'\/'
    EQUALS = r'='
    LESST = r'\<'
    GREATT = r'\>'
    LESSEQT = r'\<='
    GREATEQT = r'\>='
    COLON = r':'
    COMMA = r','
    FORALL = r'!'
    EXISTS = r'\?'
    SEMICOLON = r';'

    @_(r'\d+[\.[\d]*]?')
    def NUMBER(self, t):
        t.value = float(t.value)
        return t

    @_(r'[a-zA-Z_0-9]+')
    def VAR(self, t):
        t.type = self.reserved.get(t.value, 'VAR')  # Check for reserved words
        return t

    # Define a rule so we can track line numbers
    @_(r'\n+')
    def newline(self, t):
        self.lineno += len(t.value)

    # A string containing ignored characters (spaces and tabs)
    ignore = ' \t'

    # Error handling rule.
    def error(self, t):
        print("Illegal character '%s'" % t.value[0])
        
        input_as_array = self.user_input.split("\n")
        print(input_as_array[t.lineno - 1])
        print(" " * (t.index - 1) + "^")

        raise Exception()

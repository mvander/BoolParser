# proposition_parser.py
#
# Proposition parser is a sly implementation that handles the parsing of propositional logic statements.

from sly import Parser
from bool_lexer import BoolLexer
import pdb


class PropositionParser(Parser):
    
    # Debug files.
    debugfile = 'parser.out'

    # Grab tokens list from lexer.
    tokens = BoolLexer.tokens

    # Create a parser and store the user input inside it for better error handling.
    def parse_string(self, user_input):
        self.user_input = user_input
        lexer = BoolLexer()
        return self.parse(lexer.lex(user_input))

    # Precedence rules (further down in the table, higher precedence)
    precedence = (
        ('nonassoc', 'EQUALS', 'LESST', 'LESSEQT', 'GREATEQT', 'GREATT', 'IMPLIES'),
        ('left', 'OR', 'NEGATE', 'PLUS'),
        ('left', 'MULTIPLY', 'DIVIDE', 'AND'),
        ('right', 'UMINUS','NOT')
    )

    
    # Statements form the basis of the different types of statements the generated AST supports.

    @_('propositions')
    def statements(self, p):
        return ('STATEMENT', p.propositions)

    
    # PROPOSITIONS - one or multiple propositional statements as provided.

    @_('proposition')
    def propositions(self, p):
        return ('PROPOSITION', p.proposition)

    @_('proposition COMMA propositions')
    def propositions(self, p):
        return ('PROPOSITIONS', p.proposition, p.propositions)

    
    # PROPOSITION - single propositional statement (can be logical combinations of single statements):

    @_('boolean')
    def proposition(self, p):
        return p.boolean

    @_('variable')
    def proposition(self, p):
        return ('PROP_VAR', p.variable)

    @_('NOT proposition')
    def proposition(self, p):
        return ('PROP_NOT', p.proposition)
    
    @_('LPARENTH proposition RPARENTH')
    def proposition(self, p):
        return p.proposition

    @_('proposition AND proposition',
       'proposition OR proposition',
       'proposition IMPLIES proposition',
       'proposition EQUALS proposition')
    def proposition(self, p):
        return ('PROPS_' + str(p[1]), p.proposition0, p.proposition1)

    
    # BOOLEAN - A simple statement that has a value of true or false.

    @_('expression LESST expression',
       'expression LESSEQT expression',
       'expression GREATT expression',
       'expression GREATEQT expression',
       'expression EQUALS expression')
    def boolean(self, p):
        return (str(p[1]), p.expression0, p.expression1)

    @_('TRUE',
       'FALSE')
    def boolean(self, p):
        if p[1] == 'true':
            return ('BOOLEAN', True)
        else:
            return ('BOOLEAN', False)

    
    # EXPRESSIONS - simple addition and subtraction that can be used in a simple boolean statement.

    @_('NEGATE expression %prec UMINUS')
    def expression(self, p):
        return ('NEGATE', p.expression)

    @_('expression PLUS expression',
       'expression NEGATE expression',
       'expression MULTIPLY expression',
       'expression DIVIDE expression')
    def expression(self, p):
        return (str(p[1]), p.expression0, p.expression1)

    @_('LPARENTH expression RPARENTH')
    def expression(self, p):
        return p.expression

    @_('NUMBER')
    def expression(self, p):
        return ('NUMBER', p[0])


    # VARIABLE:

    @_('VAR')
    def variable(self, p):
        return ('VARIABLE', p[0])


    # ERROR:
    
    def error(self, p):
        # Because statements are comma separated, we can forward to the next statement (i.e., everything after the next comma)
        # and keep parsing.
        
        input_as_array = self.user_input.split('\n')

        if p:
            print('Syntax error. Unexpected token found:')
            print(input_as_Array[p.lineno - 1])
            print(' ' * (p.index-1) + "^")

            while True:
                tok = next(self.tokens, None)
                if not tok or tok.type == 'COMMA':
                    break
            self.restart()
        else:
            print('Syntax error at EOF. Statement not completed.')
            


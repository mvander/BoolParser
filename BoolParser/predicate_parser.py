# predicate_parser.py
#
# This file contains the parser for first-order logic statements.
# Much of the design is based on the following tutorial: http://www.dabeaz.com/ply/ply.html

import ply.yacc as yacc
import bool_lexer
from bool_lexer import tokens
import pdb


# Precedence rules (further down in the table, higher precedence)
precedence = (
    ('nonassoc', 'EQUALS', 'LESST', 'LESSEQT', 'GREATEQT', 'GREATT', 'IMPLIES'),
    ('left', 'OR', 'NEGATE', 'PLUS'),
    ('left', 'MULTIPLY', 'DIVIDE', 'AND'),
    ('right','NOT', 'UMINUS')
)


# Multiple types of predicates may be realized, thus we abstract to "statement" first.
def p_statements(p):
    '''statement : predicate'''
    p[0] = ('STATEMENT', p[1])


# PREDICATES - the basis of our different types of first-order logic statements.


# Fully defined predicate - specify functions, variables, and constraints.
def p_predicate_statement_functions(p):
    '''predicate : functions SEMICOLON quantifiers COLON constraints'''
    p[0] = ('PREDICATE_FUNCTIONS', p[3], p[5], p[1])


# Specify variables but not functions.
def p_predicate_statement(p):
    '''predicate : quantifiers COLON constraints'''
    p[0] = ('PREDICATE', p[1], p[3])


# Specify functions but not variables.
def p_predicate_statement_function(p):
    '''predicate : functions SEMICOLON constraints'''
    p[0] = ('PREDICATE_JUST_FUNCTIONS', p[1], p[3])


# Partial specification of variables (i.e., Forall or Exists on them, but not type - allow for uninterpreted domains).
def p_predicate_uninterp(p):
    '''predicate : part_quantifiers COLON constraints'''
    p[0] = ('UNINTERP_PREDICATE', p[1], p[3])


# Partial specification of variables with formula specifications.
def p_predicate_uninterp_funcs(p):
    '''predicate : functions SEMICOLON part_quantifiers COLON constraints'''
    p[0] = ('UNINTERP_PREDICATE_FUNCTIONS', p[3], p[5], p[1])


# Simplest definition of a predicate - simply a constraint, and interpret it ourselves.
def p_predicate_constraint(p):
    '''predicate : constraints'''
    p[0] = ('CONSTRAINT_PREDICATE', p[1])


# PREDICATE COMBINATIONS:


def p_predicate_parentheses(p):
    '''predicate : LPARENTH predicate RPARENTH'''
    p[0] = p[2]


def p_predicates_combine_logical(p):
    '''predicate : predicate IMPLIES predicate
                 | predicate AND predicate
                 | predicate OR predicate'''
    p[0] = ('PREDICATES_' + str(p[2]), p[1], p[3])


def p_predicate_not(p):
    '''predicate : NOT predicate'''
    p[0] = ('PREDICATE_!', p[2])


# FUNCTION SPECIFICATIONS:


def p_functions_multiple(p):
    '''functions : function COMMA functions'''
    p[0] = ('FUNCS', p[1], p[3])


def p_functions(p):
    '''functions : function'''
    p[0] = p[1]


def p_function(p):
    '''function : VAR COLON range ARROW range'''
    p[0] = ('FUNC_1', p[1], p[3], p[5])


def p_zero_arity_function(p):
    '''function : VAR COLON ARROW range'''
    p[0] = ('FUNC_0', p[1], p[4])


# QUANTIFIERS:


def p_quantifiers(p):
    '''quantifiers : quantifier'''
    p[0] = p[1]


def p_quantifiers_multiple(p):
    '''quantifiers : quantifier COMMA quantifiers'''
    p[0] = ('QUANTIFIERS', p[1], p[3])


def p_quantifier(p):
    '''quantifier : FORALL variable IN range
                  | EXISTS variable IN range'''
    p[0] = ('QUANTIFIER', p[1], p[2], p[4], (p.lineno(1), p.lexpos(1)))


# The following are uninterpreted predicates - that is, their functions/variables are not explicitly defined.
# Thus we start with partial quantifiers that are not strongly typed.
def p_partial_quantifier(p):
    '''part_quantifier : FORALL variable
                       | EXISTS variable'''
    p[0] = ('PARTIAL_QUANTIFIER', p[1], p[2], (p.lineno(1), p.lexpos(1)))


def p_partial_quantifiers(p):
    '''part_quantifiers : part_quantifier'''
    p[0] = p[1]


def p_partial_quantifiers_multiple(p):
    '''part_quantifiers : part_quantifier COMMA part_quantifiers'''
    p[0] = ('PARTIAL_QUANTIFIERS', p[1], p[3])


# CONSTRAINTS:


def p_constraints_multiple(p):
    '''constraints : constraint COMMA constraints'''
    p[0] = ('CONSTRAINTS', p[1], p[3])


def p_constraints(p):
    '''constraints : constraint'''
    p[0] = p[1]


# This represents a function call if it is a predicate function (i.e., returns a boolean).
def p_constraint_funccall(p):
    '''constraint : funccall'''
    p[0] = ("CONST_FUNC_CALL", p[1])


def p_constraint_varexpr(p):
    '''constraint : varexpr LESST varexpr
                  | varexpr LESSEQT varexpr
                  | varexpr GREATT varexpr
                  | varexpr GREATEQT varexpr
                  | varexpr EQUALS varexpr'''
    p[0] = ('CONST_RR_' + str(p[2]), p[1], p[3])


# Below lets us combine constraints logically (note and is implied if comma-separated constraints).

def p_constraints_combine_logical(p):
    '''constraint : constraint AND constraint
                  | constraint OR constraint
                  | constraint IMPLIES constraint'''
    p[0] = ('CONST_LOG_' + str(p[2]), p[1], p[3])


def p_constraints_logical_not(p):
    '''constraint : NOT constraint'''
    p[0] = ('CONST_LOG_!', p[2])


def p_constraints_parentheses(p):
    '''constraint : LPARENTH constraint RPARENTH'''
    p[0] = p[2]


# FUNCTION CALLS:


def p_funccall_var(p):
    '''funccall : variable LPARENTH variable RPARENTH
                | variable LPARENTH varexpr RPARENTH'''
    p[0] = ("FUNCCALL_VAR", p[1], p[3], (p.lineno(2), p.lexpos(2)))


def p_funccall_constant(p):
    '''funccall : variable LPARENTH boolean RPARENTH'''
    p[0] = ("FUNCCALL_CONSTANT", p[1], p[3], (p.lineno(2), p.lexpos(2)))


def p_funccall_zero_arity(p):
    '''funccall : variable LPARENTH RPARENTH'''
    p[0] = ("FUNCCALL_ZERO_ARITY", p[1], (p.lineno(2), p.lexpos(2)))


def p_funccall_funccall(p):
    '''funccall : variable LPARENTH funccall RPARENTH'''
    p[0] = ("FUNCCALL_IN_FUNCCALL", p[1], p[3], (p.lineno(2), p.lexpos(2)))


# VARIABLE EXPRESSIONS - this represents any expressions/expressions involving variables.


def p_var_expr_number(p):
    'varexpr : NUMBER'
    p[0] = ("NUMBER", p[1])


def p_varexpr_var(p):
    '''varexpr : variable'''
    p[0] = ("VAREXPR_VAR", p[1])


def p_varexpr_funccall(p):
    '''varexpr : funccall'''
    p[0] = ("VE_FUNC_CALL", p[1])


def p_varexpr_parentheses(p):
    '''varexpr : LPARENTH varexpr RPARENTH'''
    p[0] = p[2]


def p_varexpr_and_varexpr(p):
    '''varexpr : varexpr PLUS varexpr
               | varexpr NEGATE varexpr
               | varexpr MULTIPLY varexpr
               | varexpr DIVIDE varexpr'''
    p[0] = ("VE_COMB_VE_" + str(p[2]), p[1], p[3])


def p_negate_varexpr(p):
    '''varexpr : NEGATE varexpr %prec UMINUS'''
    p[0] = ("VAREXPR_NEGATE", p[2])


# BASIC TYPES - define a few basic types, such as range for representing a function domain/variable type and booleans
# and variables.


def p_variable(p):
    '''variable : VAR'''
    p[0] = ('VARIABLE', p[1])


def p_boolean(p):
    '''boolean : TRUE
               | FALSE'''
    if p[1] == 'true':
        p[0] = ('BOOLEAN', True)
    else:
        p[0] = ('BOOLEAN', False)


def p_range(p):
    '''range : INT
             | REAL
             | BOOL
             '''
    p[0] = ('RANGE', p[1])


# ERROR:


def p_error(p):
    # If error occurs during the string, we're pretty much done for because they only write one full statement, so we
    # raise an exception after the error message.
    if p:
        print("Syntax error. Unexpected token found:")
        print(input_as_array[p.lineno - 1])
        print(" " * (p.lexpos - 1) + "^")

        raise Exception()
    else:
        print("Syntax error at EOF. Statement not completed.")


# BUILD AND USE PARSER:


# Store user input as global to be used in error handling above.
input_as_array = None


# Build the parser
def get_parser():
    bool_lexer.get_lexer()
    return yacc.yacc(tabmodule="predicate_tab")


def parse(user_input):
    """
    Parse user input and return the Abstract Syntax Tree.
    :param user_input: String inputted by the user.
    :return: AST for the input.
    """
    # Get the lines of input to be used during error handling.
    global input_as_array
    input_as_array = user_input.split("\n")

    parsed = get_parser().parse(user_input)

    return parsed

# proposition_parser.py
#
# Proposition parser is a ply implementation that handles the parsing of propositional logic statements.

import ply.yacc as yacc
import bool_lexer
from bool_lexer import tokens
import pdb


# Precedence rules (further down in the table, higher precedence)
precedence = (
    ('nonassoc', 'EQUALS', 'LESST', 'LESSEQT', 'GREATEQT', 'GREATT', 'IMPLIES'),
    ('left', 'OR', 'NEGATE', 'PLUS'),
    ('left', 'MULTIPLY', 'DIVIDE', 'AND'),
    ('right', 'UMINUS','NOT')
)

# Statements form the basis of the different types of statements the generated AST supports


def p_statements(p):
    '''statements : propositions'''
    p[0] = ('STATEMENT', p[1])


# PROPOSITIONS - one or multiple propositional statements as provided.


def p_propositions(p):
    '''propositions : proposition'''
    p[0] = ("PROPOSITION", p[1])


def p_propositions_multiple(p):
    '''propositions : proposition COMMA propositions'''
    p[0] = ("PROPOSITIONS", p[1], p[3])


# PROPOSITION - single propositional statement (can be logical combinations of single statemenst):


def p_boolean_to_proposition(p):
    '''proposition : boolean'''
    p[0] = p[1]


def p_propositional_var(p):
    '''proposition : variable'''
    p[0] = ("PROP_VAR", p[1])


def p_propositional_not_propositional(p):
    '''proposition : NOT proposition'''
    p[0] = ('PROP_NOT', p[2])


def p_propositional_parenth(p):
    '''proposition : LPARENTH proposition RPARENTH'''
    p[0] = p[2]


def p_propositional_combine(p):
    '''proposition : proposition AND proposition
                   | proposition OR proposition
                   | proposition IMPLIES proposition
                   | proposition EQUALS proposition'''
    p[0] = ('PROPS_' + str(p[2]), p[1], p[3])


# BOOLEAN - A simple statement that has a value of true or false.


def p_expression_comparison(p):
    '''boolean : expression LESST expression
               | expression LESSEQT expression
               | expression GREATT expression
               | expression GREATEQT expression
               | expression EQUALS expression'''
    p[0] = (p[2], p[1], p[3])


def p_boolean(p):
    '''boolean : TRUE
               | FALSE'''
    if p[1] == 'true':
        p[0] = ('BOOLEAN', True)
    else:
        p[0] = ('BOOLEAN', False)


# EXPRESSIONS - simple addition and subtraction that can be used in a simple boolean statement.


def p_expr_uminus(p):
    'expression : NEGATE expression %prec UMINUS'
    p[0] = ('NEGATE', p[2])


def p_expression(p):
    '''expression : expression PLUS expression
                  | expression NEGATE expression
                  | expression MULTIPLY expression
                  | expression DIVIDE expression'''
    p[0] = (str(p[2]), p[1], p[3])


def p_expression_parenth(p):
    '''expression : LPARENTH expression RPARENTH'''
    p[0] = p[2]


def p_number(p):
    '''expression : NUMBER'''
    p[0] = ('NUMBER', p[1])

# VARIABLE:


def p_variable(p):
    '''variable : VAR'''
    p[0] = ('VARIABLE', p[1])


# ERROR:


def p_error(p):
    global parser
    global error_occurred

    error_occurred = True

    # Because statements are comma separated, we can forward to the next statement (i.e., everything after the next
    # comma) and keep parsing.
    if p:
        print("Syntax error. Unexpected token found:")
        print(input_as_array[p.lineno - 1])
        print(" " * (p.lexpos - 1) + "^")

        while True:
            tok = parser.token()  # Get next token.
            if not tok or tok.type == 'COMMA':
                break
        parser.restart()
    else:
        print("Syntax error at EOF. Statement not completed.")


# Store parser in global var so it can be used in the error handling above.
parser = None


# Store user input as global to be used in error handling above.
input_as_array = None


# Notify that error occured so we don't let them use a partly parsed result.
error_occurred = False


# Build the parser
def get_parser():
    global parser
    bool_lexer.get_lexer()
    parser = yacc.yacc(tabmodule="proposition_tab")
    return parser


def parse(user_input):
    """
    Parse user input and return the Abstract Syntax Tree.
    :param user_input: String inputted by the user.
    :return: AST for the input.
    """
    # Get the lines of input to be used during error handling.
    global input_as_array
    input_as_array = user_input.split("\n")

    parsed = get_parser().parse(user_input)

    global error_occurred
    if error_occurred:
        error_occurred = False
        return None
    else:
        return parsed

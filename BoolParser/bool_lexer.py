# bool_lexer.py
#
# This file contains the lexer for our parsers, and is used for parsing both propositional and first-order logic
# statements.

import ply.lex as lex

# Reserved keywords
reserved = {
    'true': 'TRUE',
    'false': 'FALSE',
    'Z': 'INT',
    'R': 'REAL',
    'Bool' : 'BOOL',
    'in': 'IN'
}

# Valid tokens
tokens = [
    'NEGATE',
    'NOT',
    'AND',
    'OR',
    'PLUS',
    'IMPLIES',
    'VAR',
    'NUMBER',
    'LPARENTH',
    'RPARENTH',
    'MULTIPLY',
    'DIVIDE',
    'EQUALS',
    'LESST',
    'GREATT',
    'LESSEQT',
    'GREATEQT',
    'COLON',
    'COMMA',
    'FORALL',
    'EXISTS',
    'SEMICOLON',
    'ARROW'
] + list(reserved.values())

# Regex for simple tokens:
t_NEGATE    = r'-'
t_NOT       = r'\~'
t_AND       = r'\&'
t_OR        = r'\|'
t_PLUS      = r'\+'
t_IMPLIES   = r'\=\>'
t_LPARENTH  = r'\('
t_RPARENTH  = r'\)'
t_MULTIPLY  = r'\*'
t_DIVIDE    = r'\/'
t_EQUALS    = r'='
t_LESST     = r'\<'
t_GREATT    = r'\>'
t_LESSEQT   = r'\<='
t_GREATEQT  = r'\>='
t_COLON     = r':'
t_COMMA     = r','
t_FORALL    = r'!'
t_EXISTS    = r'\?'
t_SEMICOLON = r';'
t_ARROW     = r'->'


def t_NUMBER(t):
    r'\d+[\.[\d]*]?'
    t.value = float(t.value)
    return t


def t_VAR(t):
    r'[a-zA-Z_0-9]+'
    t.type = reserved.get(t.value,'VAR')    # Check for reserved words
    return t


# Define a rule so we can track line numbers
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)


# A string containing ignored characters (spaces and tabs)
t_ignore = ' \t'


# Error handling rule.
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)


def get_lexer():
    return lex.lex()

# def_bool.py
#
# This file contains the interface with the PLY boolean parser defined in predicate_parser.py specifically
# for use with Jupyter Notebooks. This is the backend, supplying the following functionality:
# - BDDs for propositions
# - Solve simple boolean expressions.
# - Solve predicate logic/propositions w/ Z3 solver.

from z3 import *
import sys
import pdb

sys.path.append("../../../PBDD/include/")
sys.path.append("../../../PBL/include/")
import BDD as bdd
import predicate_parser
import proposition_parser


class Statement:
    """
    Statement acts as a class to store data for user input. Will be used to store data that different backends can
    use.
    """

    def __init__(self, string, parsed_info):
        """
        Provide the original statements as a string and provide the parsed version.
        """
        self.string = string
        self.parsed = parsed_info
        self.vars = []

    def __str__(self):
        return str(self.string)

    def add_variable(self, var_name):
        """
        Add the provided variable to list of variables in statement.
        """
        if not self.vars.__contains__(var_name):
            self.vars.append(var_name)

    def get_variables(self):
        """
        Return the set of variables in the given boolean statement.
        """
        return self.vars


class Proposition(Statement):
    """
    Proposition is a statement that involves boolean variables. Can be used to create a BDD diagram (and eventually
    you'll be able to generate truth tables too).
    """

    def __init__(self, string, parsed_info, statements):
        """
        Provide original statements as string and provide the parsed version.
        """
        self.statements = statements
        # Forward rest to Statement's constructor.
        Statement.__init__(self, string, parsed_info)
        self.bdd = self._build_bdd()

    def create_bdd(self, var_order=None):
        """
        Return the Binary Decision Diagram (graphviz form) for this proposition.
        NOTE: This is not optimized (yet) for variable order. That is still to come.
        """

        if var_order is not None:
            bdd_current = self._build_bdd(var_order)
        else:
            bdd_current = self.bdd

        return bdd.dot_bdd(bdd_current)

    def truth_table(self):
        """
        Print the truth table for the proposition statement.
        """
        combinations = self._build_combinations([], [], 0)
        truth_table = {}
        true_combinations = bdd.all_sat(self.bdd)

        # Go through all the combinations possible and see if they are in the set of combinations that return true.
        for combination in combinations:
            if combination in true_combinations:
                truth_table[str(combination).replace("[", "").replace("]", "").replace("'", "")] = 1
            else:
                truth_table[str(combination).replace("[", "").replace("]", "").replace("'", "")] = 0

        return truth_table

    def _build_combinations(self, current_list, table_list, var_num):
        """
        Generate all possible truth values for the list of variables via recursion.
        """
        # Add the next var to the current list.
        current_list.append(self.vars[var_num])

        # If the variable num we are on is equal to the length of the variables, we have a combination of boolean values
        # to be added to the truth table.
        if var_num is (len(self.vars)-1):
            table_list.append(current_list)
        else:
            table_list = self._build_combinations(list(current_list), list(table_list), var_num+1)

        # Now swap the current var with its not value.
        current_list = list(current_list)
        current_list.remove(self.vars[var_num])
        current_list.append("~" + self.vars[var_num])

        # If the variable num we are on is equal to the length of the variables, we have a combination of boolean values
        # to be added to the truth table.
        if var_num is (len(self.vars) - 1):
            table_list.append(current_list)
        else:
            table_list = self._build_combinations(list(current_list), list(table_list), var_num + 1)

        return table_list

    def _build_bdd(self, var_order=None):
        """
        Build the bdd object using PBDD.
        """
        # We have to do this to make sure we've parsed out the variables.
        _solve(self, self.parsed, Solver(), {})

        main_expr = "Main_Exp : " + self._create_main_exp().replace(".", "&").replace("+", "|") \
            .replace("\n", "")

        if var_order is None:
            # Grab our variable order
            var_order = self.get_variables()

        var_order = "Var_Order : " + str(var_order).replace(",", "").replace("[", "").replace("]", "") \
            .replace("'", "")

        # Create and set this object's bdd.
        return bdd.bdd_create(main_expr + "\n" + var_order)

    def _create_main_exp(self):
        # Split the string into the different provided statements, then filter out an empty statements.
        statements = self.string.split("\n")
        statements = filter(lambda x: x.strip() != "", statements)
        # Combine them by and-ing.
        statements = map(lambda x: x + ".", statements)
        string = "".join(statements)
        # Return everything except the last character which will be an extra and.
        return string[:(len(string) - 1)]


# Public interface methods:

def create_predicate(input):
    """
    Given a string provided from jupyter notebook, parse it as a first-order-logic statement and store the information.
    They can use the other methods defined below to actually solve the expression.
    :param input: The string input from the user.
    :return: A statement object containing the information provided.
    """
    # Get the lines of input to be used during error handling.
    input_as_array = input.split("\n")

    parser = predicate_parser.get_parser()
    # Parse input w/ PLY parser.
    try:
        parsed = parser.parse(input)
    except Exception:
        return

    # Debugging statements:
    # print(parsed)

    return Statement(input, parsed)


def create_proposition(input):
    """
    Given a string provided from jupyter notebook, parse it as a propositional statement and store the information.
    They can use the other methods defined below to actually solve the expression.
    :param input: The string input from the user.
    :return: A statement object or a propositional object (subclass that allows creation of Truth Tables and BDDs).
    """
    # Parse input w/ PLY parser.
    parsed = proposition_parser.parse(input)

    # Make sure that if the input didn't parse we don't let them keep going.
    if parsed is None:
        return

    # Get the individual statements.
    statements = _get_statements(parsed, [])

    # Debugging statements:
    # print(parsed)
    # print(statements)

    # Determine type of statement. This is used to differentiate propositions, who have added functionality (BDD).
    statement_type = ""
    for index in range(len(statements)):
        current_type = _type_of_statement(statements[index])
        if statement_type is not "" and current_type is not statement_type:
            raise Exception("Statements should be of the same type.")
        statement_type = current_type

    # If we don't raise an error than at this point, type1 and type2 are the same and hold the type of our statement.
    if statement_type is "proposition":
        s = Proposition(input, parsed, statements)
    else:
        s = Statement(input, parsed)

    return s


def evaluate(statement):
    """
    Given a statement, evaluate and print it's value to the user. For items using Z3, print the Z3 satisfiability and
    if it is satisfiable, print the generated model.
    """
    s = Solver()

    # Get the lines of input to be used during error handling.
    input_as_array = statement.string.split("\n")

    try:
        ans = _solve(statement, statement.parsed, s, {})
    except Exception as e:
        print(e.args[0])
        if e.args[1] is not None:
            lineno, loc = e.args[1]
            print(input_as_array[lineno - 1])
            print(" " * (loc - 1) + "^")
        return

    # If numscopes is greater than zero, we have a solution waiting in the Z3 solver.
    print(s.check())
    if str(s.check()) == 'sat':
        # print(_interpret_z3_solution(s.model(), varmap))
        print(str(s.model()))
    return s


def valid(statement):
    """
    Given a statement, if it has a solver, not it, then check the satisfaction. If it satisfiable, the original is
    not valid, if it is not satisfiable, the original is.
    """
    s = Solver()

    # Get the lines of input to be used during error handling.
    input_as_array = statement.string.split("\n")

    try:
        ans = _solve(statement, statement.parsed, s, {})
    except Exception as e:
        print(e.args[0])
        if e.args[1] is not None:
            lineno, loc = e.args[1]
            print(input_as_array[lineno - 1])
            print(" " * (loc - 1) + "^")
        return

    # If numscopes is greater than zero, we have a solution waiting in the Z3 solver.
    s.reset()
    s.add(Not(ans))
    if str(s.check()) == 'sat':
        print("False")
    else:
        print("True")
    # Return the solver so they can check out the cases provided by Z3 that make the Not true.
    return s

# Private helper methods.


def _get_statements(expression_tuple, statements):
    """
    Given the parsed tuple of a boolean expression, recursively build a list of the individual statements.
    """
    if expression_tuple[0] is "STATEMENTS":
        statements.append(expression_tuple[1])
        return _get_statements(expression_tuple[2], statements)
    elif expression_tuple[0] is "STATEMENT":
        statements.append(expression_tuple[1])
        return statements


def _type_of_statement(statement):
    """
    Given a statement return the type of the statement as either a normal statement or a proposition.
    """
    # It's possible the statement contains a statement, in which case we must recurse to find the type of the overall
    # statement.
    if statement[1][0] is "STATEMENT":
        return _type_of_statement(statement[1])
    # Note here we do not include when we set a variable or proposition equal to another one, that is because
    # the main inspiration for typing is to allow our syntax to work with the PBDD library.
    elif "PROP" in statement[1][0] and "PROPS_=" not in statement[1][0]:
        return "proposition"
    else:
        return "boolean"


# Custom sort that can be used for solver below.
A = DeclareSort('A')


def _solve(s, operation, solver, varmap):
    """
    Solves by recursively going through the parsed tuple resulting from the given boolean expression.

    :param s: The relevant statement object.
    :param operation: the tuple representing the current location in the AST we are performing
    :param solver: Z3 solver we continually add to.
    :param varmap: Dictionary mapping from variable name to variable type.
    :return: Solution, true/false for a boolean expression, otherwise we look to the solver object.
    """
    op = operation[0]

    if op == "STATEMENTS":
        return [_solve(s, operation[1], solver, varmap), _solve(s, operation[2], solver, varmap)]
    elif op == "STATEMENT":
        return _solve(s, operation[1], solver, varmap)
    elif op == "EXPRESSION" or op == "TERM":
        return _solve(s, operation[1], solver, varmap)
    elif op == "NUMBER":
        return operation[1]
    elif op == "FACTOR":
        # It is possible that an expression is surrounded by parentheses, in which case here we will have an expression
        # to decode.
        if type(operation[1]) is not tuple:
            return float(operation[1])
        else:
            return _solve(s, operation[1], solver, varmap)
    elif op == "NEGATE":
        return -operation[1]
    elif op == "+":
        return _solve(s, operation[1], solver, varmap) + _solve(s, operation[2], solver, varmap)
    elif op == "-":
        return _solve(s, operation[1], solver, varmap) - _solve(s, operation[2], solver, varmap)
    elif op == "*":
        return _solve(s, operation[1], solver, varmap) * _solve(s, operation[2], solver, varmap)
    elif op == "/":
        return _solve(s, operation[1], solver, varmap) / _solve(s, operation[2], solver, varmap)
    elif op == "BOOLEAN":
        return operation[1]
    elif op == "OR":
        return _solve(s, operation[1], solver, varmap) or _solve(s, operation[2], solver, varmap)
    elif op == "AND":
        return _solve(s, operation[1], solver, varmap) and _solve(s, operation[2], solver, varmap)
    elif op == "=":
        return _solve(s, operation[1], solver, varmap) == _solve(s, operation[2], solver, varmap)
    elif op == "IMPLIES":
        return not _solve(s, operation[1], solver, varmap) or _solve(s, operation[2], solver, varmap)
    elif op == "<":
        return _solve(s, operation[1], solver, varmap) < _solve(s, operation[2], solver, varmap)
    elif op == ">":
        return _solve(s, operation[1], solver, varmap) > _solve(s, operation[2], solver, varmap)
    elif op == "<=":
        return _solve(s, operation[1], solver, varmap) <= _solve(s, operation[2], solver, varmap)
    elif op == ">=":
        return _solve(s, operation[1], solver, varmap) >= _solve(s, operation[2], solver, varmap)
    elif op == "VARIABLE":
        # Keep track of used variables (helpful for BDD).
        s.add_variable(operation[1])
        return operation[1]  # Return the var name.
    elif op == "CONSTRAINTS":
        constraint1 = _solve(s, operation[1], solver, varmap)
        constraints = _solve(s, operation[2], solver, varmap)
        # Make a list of the constraints.
        if type(constraints) is list:
            constrs = [constraint1]
            constrs.extend(constraints)
            return constrs
        else:
            # If constraints is not a list it means there is only one in which case it returns a Z3 constraint object.
            return [constraint1, constraints]
    elif op == "CONST_FUNC_CALL":
        return _function_call_interpret(operation[1], varmap, True, s, solver)
    elif "CONST_" in op:
        # Generate relevant items.
        x = _solve(s, operation[1], solver, varmap)
        # There is one case where we don't need the second one which is 'Not'ing a constraint - we handle that here.
        if op == "CONST_LOG_!":
            return Not(x)
        y = _solve(s, operation[2], solver, varmap)

        # Build constraint.
        const = _solver_const(op, x, y)

        return const
    elif "VAREXPR_VAR" in op:
        varname = _solve(s, operation[1], solver, varmap)
        if varname not in varmap:
            # Add it to varmap with new Real interpretation based on context.
            varmap[varname] = "REAL"

        # Because everything internally is a real, return accordingly.
        return Real(varname)
    elif "VE_FUNC_CALL" in op:
        return _function_call_interpret(operation[1], varmap, False, s, solver)
    elif "VE_" in op:
        # Get the two items to operate.
        x = _solve(s, operation[1], solver, varmap)
        y = _solve(s, operation[2], solver, varmap)

        # Build varexpr.
        varexpr = _solver_varexpr(op, x, y)

        # Let someone else handle adding it.
        return varexpr
    elif "VAREXPR_NEGATE" == op:
        return -_solve(s, operation[1], solver, varmap)
    elif "FUNCS" == op:
        # We don't need to return anything, just add all functions into the varmap indirectly, so forward call.
        _solve(s, operation[1], solver, varmap)
        _solve(s, operation[2], solver, varmap)
    elif "FUNC_1" == op:
        domain = _solve(s, operation[2], solver, varmap)
        domain_range = _solve(s, operation[3], solver, varmap)

        # Because we represent values inside as reals, for now cast them over.
        if domain is "INT":
            domain = "REAL"

        varmap[operation[1]] = (domain, domain_range)
    elif "FUNC_0" == op:
        domain_range = _solve(s, operation[2], solver, varmap)

        # Z3 stores 0 arity functions as normal variables - Quote from Z3 tutorial: "The basic building blocks of
        # SMT formulas are constants and functions. Constants are just functions that take no arguments.
        # So everything is really just a function."
        # Thus below when we interpret zero-arity functions, we treat them as single variables of the provided
        # type.
        varmap[operation[1]] = domain_range
    elif "CONSTRAINT_PREDICATE" == op:
        constraints = _solve(s, operation[1], solver, varmap)

        f = True

        if type(constraints) is list:
            for constraint in constraints:
                f = And(constraint, f)
        else:
            f = And(constraints, f)

        solver.push()
        solver.add(f)

        return f
    elif "PREDICATE_!" in op:
        # Decode relevant predicate.
        predicate = _solve(s, operation[1], solver, varmap)

        # By def of pred, it will be in the solver, so pop once.
        solver.pop()

        # Now not and add it back to the solver.
        solver.push()
        solver.add(Not(predicate))
        return Not(predicate)
    elif "PREDICATES" in op:
        # Decode relevant predicates.
        predicate1 = _solve(s, operation[1], solver, varmap)
        predicate2 = _solve(s, operation[2], solver, varmap)

        # By definition of predicate below, both will be added to the solver, so we need to pop twice.
        solver.pop()
        solver.pop()

        # Now we do the relevant operation and add it to the solver.
        f = _solver_prop(op, predicate1, predicate2)

        # Now add to the solver.
        solver.push()
        solver.add(f)
        return f
    elif "PREDICATE_JUST_FUNCTIONS" == op:
        # Start by adding function definitions to our varmap.
        _solve(s, operation[1], solver, varmap)

        constraints = _solve(s, operation[2], solver, varmap)

        # Basic starting value that we can AND other constraints to.
        f = True

        # Build up our constraint function f by and-ing all the constraints.
        if type(constraints) is list:
            for constraint in constraints:
                f = And(constraint, f)
        # Otherwise we have a single constraint, AND it with f.
        else:
            f = And(constraints, f)

        solver.push()
        solver.add(f)
        return f
    elif "PREDICATE" in op:  # Will handle uninterpreted predicates too.
        # Go decode the functions if necessary, thus adding them to the varmap to be looked up.
        # This must happen before we try to decode the constraints.
        if "FUNCTIONS" in op:
            _solve(s, operation[3], solver, varmap)

        quantifiers = _solve(s, operation[1], solver, varmap)
        constraints = _solve(s, operation[2], solver, varmap)

        # Basic starting value that we can AND other constraints to.
        f = True

        # Build up our constraint function f by and-ing all the constraints.
        if type(constraints) is list:
            for constraint in constraints:
                f = And(constraint, f)
        # Otherwise we have a single constraint, AND it with f.
        else:
            f = And(constraints, f)

        # If we have multiple quantifiers (or later constraints) then go through and add them, otherwise, they are a
        # single quanitifer (or constraint) and we add just them.
        if type(quantifiers) is list:
            for x in range(len(quantifiers)-1, -1, -1):
                quantifier = _define_z3_vars_if_necessary(quantifiers[x], varmap)

                if quantifier[0] == "!":
                    f = ForAll(quantifier[1], f)
                else:
                    f = Exists(quantifier[1], f)
        else:
            quantifiers = _define_z3_vars_if_necessary(quantifiers, varmap)

            if quantifiers[0] == "!":
                f = ForAll(quantifiers[1], f)
            else:
                f = Exists(quantifiers[1], f)

        # Add our final function f to the solver.
        solver.push()
        solver.add(f)
        return f
    elif op == "RANGE":
        domain_range = operation[1]

        if domain_range == "Z":
            return "INT"
        elif domain_range == "R":
            return "REAL"
        else:
            return "BOOL"
    elif "QUANTIFIERS" in op: # We will let this handle partial quantifiers as well.
        quantifier1 = _solve(s, operation[1], solver, varmap)
        quantifiers = _solve(s, operation[2], solver, varmap)
        # Make a list of the quantifiers.

        if type(quantifiers) is list:
            qtfrs = [quantifier1]
            qtfrs.extend(quantifiers)
            return qtfrs
        else:
            # If quantifiers is not a list it means there is only one in which case it returns a quantifier definition.
            return [quantifier1, quantifiers]
    elif op == "PARTIAL_QUANTIFIER":
        varname = str(_solve(s, operation[2], solver, varmap))

        # Return partial quantifier as just the name, we can add interpretation when dealing with the constraints.
        return operation[1], varname
    elif op == "QUANTIFIER":
        varname = str(_solve(s, operation[2], solver, varmap))

        if _solve(s, operation[3], solver, varmap) != "Bool":
            var_type = _solve(s, operation[3], solver, varmap)

            if varname in varmap:
                if varmap[varname] != var_type:
                    raise Exception("Variable " + str(varname) + " defined differently in single statement! Please use "
                                                                 "different variable names.", operation[4])

            varmap[varname] = var_type
        else:  # They are using booleans, which doesn't make sense. That range exists only for return type.
            raise Exception("Range type Bool not valid as a quantified variable.", operation[4])

        # Note that we represent Ints as Reals as well. This helps provide accurate values as just doing Ints limits
        # accuracy.
        x = Real(varname)

        # Return the quantifier type (FORALL or EXISTS) and associated variable.
        return operation[1], x
    elif op == "PROPOSITIONS":
        proposition1 = _solve(s, operation[1], solver, varmap)

        # Add the proposition.
        solver.add(proposition1)

        # Call solve on the following propositions so that they too are added.
        _solve(s, operation[2], solver, varmap)
        solver.push()
        return proposition1
    elif op == "PROPOSITION":
        proposition = _solve(s, operation[1], solver, varmap)

        # Add the proposition.
        solver.add(proposition)
        solver.push()
        return proposition
    elif op == "PROP_VAR":
        bool_var = Bool(_solve(s, operation[1], solver, varmap))

        return bool_var
    elif op == "PROP_NOT":
        to_not = _solve(s, operation[1], solver, varmap)

        return Not(to_not)
    elif "PROPS_" in op:
        x = _solve(s, operation[1], solver, varmap)
        y = _solve(s, operation[2], solver, varmap)

        # Generate the relevant proposition from multiple propositions.
        prop = _solver_prop(op, x, y)

        return prop
    else:
        return ""


def _define_z3_vars_if_necessary(quantifier, varmap):
    """
    It is possible that in going through the constraints, we have attached a definition to variable types, so we would
    have to check for this and attach the meaning accordingly.

    :param quantifier: Quantifier represented as tuple.
    :param varmap: Variable map that maps from variable to its type.
    :return: return the changed quantifier tuple.
    """
    quantifier_type = quantifier[0]
    quantifier_actual = quantifier[1]

    # It is possible that the user used partial quantifiers, which means that we would need to actually
    # define the Z3 variable here.
    if type(quantifier[1]) is not ArithRef:

        # Note can only be arithref, otherwise forall/exists doesn't make sense.
        if quantifier[1] in varmap:
            var_name = varmap[quantifier[1]]
            if var_name == "INT" or var_name == "REAL":
                quantifier_actual = Real(quantifier[1])
            elif var_name == "A":
                quantifier_actual = Const(quantifier[1], A)
            else:
                raise Exception("Interpretation for " + quantifier[1] + " does not make "
                                                                         "sense with ForAll/Exists.", quantifier[4])
        else:
            raise Exception("No interpretation for variable " + quantifier[1] + " inferred.", quantifier[4])

    return quantifier_type, quantifier_actual


def _function_call_interpret(func_call, varmap, constraint, s, solver):
    """
    Given the parsed function call, decide what the interpretation of that call should be.

    :param func_call: Parsed function call.
    :param varmap: dictionary mapping from variable name to variable type.
    :param constraint: Boolean indicating if this is called as a constraint. If not, we will treat it as a
    variable expression, the difference being that a constraint returns a boolean and a variable expression returns
    a real.
    :param s: Forwarded statement from our normal solve call (we may need to call solve in here).
    :param solver: Forwarded Z3 solver from our normal solve call (we may need to call solve in here).
    :return: The Z3 function call.
    """
    # Get the function name.
    func_name = _solve(s, func_call[1], solver, varmap)

    # First check if it's a zero arity, and if it is, just handle it here.
    if func_call[0] == "FUNCCALL_ZERO_ARITY":
        # If it's in the varmap, they provided a function. Check to make sure they use it correctly in context, then
        # return it's Z3 variable.
        if func_name in varmap:
            domain_range = varmap[func_name]

            _return_type_check(constraint, domain_range, func_name, func_call)

            if domain_range == "BOOL":
                return Bool(func_name)
            elif domain_range == "INT" or domain_range == "REAL":
                return Real(func_name)
        else:
            # If it's not in the varmap, provide interpretation based on surroundings - this means they didn't define
            # the function.
            if constraint:
                return Bool(func_name)
            else:
                return Real(func_name)

    if func_call[0] == "FUNCCALL_IN_FUNCCALL":
        # If the function is in the varmap, they provided a function. Check to make sure they use it correctly, then
        # check interpretation of the function inside of it.
        if func_name in varmap:
            (domain, domain_range) = varmap[func_name]
            func = Function(func_name, _get_sort(domain), _get_sort(domain_range))

            # Make sure it is used correctly.
            _return_type_check(constraint, domain_range, func_name, func_call)

            arg = _function_call_interpret(func_call[2], varmap, domain == "BOOL", s, solver)

            return func(arg)
        else:
            # We need to supply the type. Return type will be based on whether it is a constraint or not, arg type will
            # be based on the return type of the called function (perhaps it has a function def).

            arg = _function_call_interpret(func_call[2], varmap, False, s, solver)

            if str(arg.decl().range()) == "Int" or str(arg.decl().range()) == "Real":
                arg_type = "REAL"
            elif str(arg.decl().range()) == "Bool":
                arg_type = "BOOL"

            # Now go from the arg_type to the prescribed return type of either boolean if its a constraint or real.
            if constraint:
                varmap[func_name] = (arg_type, "BOOL")
                func = Function(func_name, _get_sort(arg_type), BoolSort())
            else:
                varmap[func_name] = (arg_type, "REAL")
                func = Function(func_name, _get_sort(arg_type), RealSort())

            return func(arg)

    # Get the argument being sent - we know it will be either a constant or a variable expression (arithmetic)
    # or a lone variable
    arg = _solve(s, func_call[2], solver, varmap)

    # Stores whether the call we are dealing with was provided a constant argument or a variable.
    constant_call = func_call[0] == "FUNCCALL_CONSTANT"

    # arg_type represents the passed argument type and is not set now for uninterpreted variables.
    arg_type = None

    if str(arg) in varmap:
        # If arg is in varmap, the user has provided an interpretation for us, and we know that we were passed a lone
        # variable. We also go ahead and reassign arg to the Z3 variable version, thus providing the interpretation
        # asked for by the user.
        arg_type = varmap[str(arg)]

        if arg_type == "INT" or arg_type == "REAL":
            arg = Real(arg)
        elif arg_type == "BOOL":
            arg = Bool(arg)
        elif arg_type == "A":
            arg = Const(arg, A)
    elif type(arg) is ArithRef: # If the user provides a Variable Expression (i.e., x+1), we want to tag it as
        # interpreted.
        arg_type = "REAL"
    elif type(arg) is float: # A constant could have been sent that got parsed into a varexpr. We should notify the
        # algorithm that we will now be treating this call as a constant call.
        constant_call = True
        arg_type = "REAL"
    elif constant_call:  # Constant call will be true if we get passed a boolean value.
        arg_type = "BOOL"
    # If it is none of these, we do not force an interpretation unless context later allows us to do so.

    # Check if the function is in the varmap, if it is, the user provided an interpretation for us to use.
    if func_name in varmap:
        (domain, domain_range) = varmap[func_name]
        func = Function(func_name, _get_sort(domain), _get_sort(domain_range))

        _return_type_check(constraint, domain_range, func_name, func_call)

        if constant_call and arg_type is not None and arg_type != domain:
            if not (domain == "INT" and arg_type == "REAL" and arg.is_integer()):
                raise Exception("Argument provided to function " + func_name + " is incorrect.", func_call[3])
        elif not constant_call and arg_type is not None and arg_type != domain:
            if not (arg_type == "INT" and domain == "REAL"):
                raise Exception("Argument provided to function " + func_name + " is incorrect.", func_call[3])
        elif not constant_call and arg_type is None:
            # If arg_type is none it means we have an uninterpreted variable, now we can give it one from the function
            # definition

            varmap[arg] = domain
            if domain == "REAL" or domain == "INT":
                arg = Real(arg)
            elif domain == "BOOL":
                arg = Bool(arg)

        # Now we simply return the actual Z3 function call.
        return func(arg)
    else:
        # If the function is not in the varmap, we need to create an interpretation for the function we are using.

        # First we need to provide a type for the variable if it does not have one. In this case, we will use a custom
        # sort. NOTE: this only runs if the user hasn't provided some interpretation for the variable in question.
        if arg_type is None:
            varmap[arg] = "A"
            arg_type = "A"
            arg = Const(arg, A)

        # Because we store everything internally as reals for accuracy's sake, switch from int to Real here for the
        # domain if necessary.
        if arg_type == "INT":
            arg_type = "REAL"

        # Now go from the arg_type to the prescribed return type of either boolean if its a constraint or real.
        if constraint:
            varmap[func_name] = (arg_type, "BOOL")
            func = Function(func_name, _get_sort(arg_type), BoolSort())
        else:
            varmap[func_name] = (arg_type, "REAL")
            func = Function(func_name, _get_sort(arg_type), RealSort())

        # Now we simply return the actual Z3 function call.
        return func(arg)


def _return_type_check(constraint, domain_range, func_name, func_call):
    """
    This checks that the provided function call is used correctly where it is.
    :param constraint: Whether the funccall is used as a constraint (in which case it should return a bool).
    :param domain_range: Return of the funccall.
    :param func_name: Name of the function being called.
    """
    # If it is a constraint, make sure the return type is boolean, otherwise make sure it is real/int.
    if constraint and domain_range != "BOOL":
        raise Exception("Function " + func_name + " usage does not return a boolean!", func_call[len(func_call)-1])
    elif not constraint and domain_range != "INT" and domain_range != "REAL":
        raise Exception("Function " + func_name + " usage does not return an integer or real!",
                        func_call[len(func_call)-1])


def _get_sort(domain):
    if domain == "INT":
        return IntSort()
    elif domain == "REAL":
        return RealSort()
    elif domain == "BOOL":
        return BoolSort()
    elif domain == "A":
        return A
    else:
        raise Exception("Unexpected domain " + domain + " provided.")


def _get_variable(name, varmap):
    """
    Given the name of a variable and a map that keeps track of each variable's type, return the correct Z3 variable.
    For now we represent all internal numbers as Reals, so just leave it as this for now.
    """
    if name in varmap and varmap[name] == "A":
        return Const(name, A)
    elif name in varmap:
        return Real(name)
    else:
        return


def _solver_varexpr(op, x, y):
    """
    Based on the provided operation and two parts, generate the correct Z3 statement.
    """
    if "+" in op:
        varexpr = x + y
    elif "-" in op:
        varexpr = x - y
    elif "*" in op:
        varexpr = x * y
    else:
        varexpr = x/y
    return varexpr


def _solver_prop(op, x, y):
    """
    Based on the provide operation and two parts, generate the correct Z3 statement.
    """
    if "|" in op:
        prop = Or(x, y)
    elif "&" in op:
        prop = And(x, y)
    elif "=>" in op:
        prop = Implies(x, y)
    else:
        prop = x == y
    return prop


def _solver_const(op, x, y):
    """
    Based on the operation and provided parts, create the correct Z3 constraint.
    """
    op = op.replace("CONST_RE_", "").replace("CONST_RR_", "")

    if "=" == op :
        const = x == y
    elif "<" == op:
        const = x < y
    elif ">" == op:
        const = x > y
    elif "<=" == op:
        const = x <= y
    elif ">=" == op:
        const = x >= y
    else:
        # We can apply logical operations on our constraints, so pass that along if none of the above is the case.
        const = _solver_prop(op, x, y)
    return const

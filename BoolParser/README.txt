BoolParser - A Boolean Parser for Jupyter Notebook

The Boolean Parser is a simple interface between you and the Z3 solver, a system that, given constraints, can attempt
to solve them. The purpose of the parser is to provide you with a more convenient markdown for using the Z3 system.
Here we will describe the functions used to interact with the system from python.

The parser supports two basic types of logic: propositional and predicate (first order). The foundation of the usage
is the Statement type. Statement encapsulates a provided logic statement and stores information about it that you can
use. This object can then be passed to other calls, such as evaluate and valid, that will provide more information on
the statement using the Z3 solver.

To create statements use the following two:

create_proposition(string) - given a string propositional statement, creates a statement storing that info. Note, that
on occassion, this will return a Proposition object instead of a vanilla Statement object. This provides it with support
for BDDs and truth tables. Not all propositional statements are valid for this variety however, so some will still
return a simple Statement object.

create_predicate(string) - given a string predicate statement, creates a statement storing that info.

Once you have a statement (or proposition) object:

evaluate(statement) - given a statement, loads it into a Z3 solver and attempts to solve it. If it does, it prints out
the result, and if satisfiable, a satisfying value set. It also returns the Z3 solver so you can use it more.

valid(statement) - given a statement, determines if it is valid, by loading it's NOT into Z3 and attempting to solve it.
 This simply prints true or false, depending on its validity, and returns the Z3 solver for further use.

For more information on the valid syntax for each type of statement, please see the 'boolparser' jupyter notebook inside
the JupyterExamples folder.
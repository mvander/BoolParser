This project contains three subprojects:

BoolParser - a python interface for Z3 that provides a more convenient and intuitive markdown language to use to interact with the Z3 solver.

AdjacencyMap - simple python program that uses OpenStreetMap to generate adjacency maps and provide coloring to those maps.

PCPJupyter - python interface for a PCP instance solver.
